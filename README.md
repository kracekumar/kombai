[![PyPI Version](https://img.shields.io/pypi/v/kombai)](https://pypi.org/project/kombai/)
[![Python Versions](https://img.shields.io/pypi/pyversions/kombai)](https://pypi.org/project/kombai/)
[![Python Wheel](https://img.shields.io/pypi/wheel/kombai)](https://pypi.org/project/kombai/)
[![License](https://img.shields.io/badge/License-MIT-green.svg)](https://opensource.org/licenses/MIT)
[![Code Style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Code Quality: flake8](https://img.shields.io/badge/code%20quality-flake8-000000.svg)](https://gitlab.com/pycqa/flake8)

# kombai
Terminal based structured data viewer and editor written in Python

## Contributing
Please refer to [CONTRIBUTING.md](CONTRIBUTING.md) file for more information on how to
contribute to this project.
