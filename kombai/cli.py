import click

from kombai.renderer import render


@click.command()
@click.argument("db_uri")
def main(db_uri):
    render(db_uri=db_uri)


if __name__ == "__main__":
    main()
