from sqlalchemy import create_engine, inspect, select, func
from sqlalchemy.schema import MetaData
from dataclasses import dataclass
from typing import Any
from rich.table import Table


@dataclass
class Batch:
    columns: list[str]
    rows: list[tuple[str, ...]]
    label: str
    total: int

    def as_table(self):
        table = Table(title=f"{self.label} ({self.total} rows)", show_lines=True)
        for col in self.columns:
            table.add_column(col, overflow="fold")

        for row in self.rows:
            table.add_row(*row)

        return table


class DataReader:
    def get_table_names(self):
        return self.inspector.get_table_names()

    def get_table_rows(self, table_name: str):
        tbl = self.metadata.tables[table_name]
        stmt = select(tbl).limit(10)
        with self.engine.connect() as conn:
            rows = [tuple(str(field) for field in row)
                    for row in conn.execute(stmt)]
            return Batch(columns=tbl.c.keys(), rows=rows, label=table_name,
                         total=conn.execute(select([func.count()]).select_from(tbl)).scalar())



class DataWriter:
    pass


class DataBrowser(DataReader, DataWriter):
    def __init__(self, db_uri: str):
        self.db_uri = db_uri
        self.engine = create_engine(db_uri)
        self.inspector = inspect(self.engine)
        self.metadata = MetaData()
        self.metadata.reflect(bind=self.engine)
