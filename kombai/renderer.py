"""Render the data in the terminal
"""
from textual.app import App
import rich
from rich.console import RenderableType
from rich.table import Table
from rich.tree import Tree
from textual.widgets import ScrollView, Header, Footer, Static, TreeNode, TreeControl, TreeClick, Placeholder
from textual.message import Message

from kombai import db
from pathlib import Path


class Browser(App):
    def __init__(self, **kwargs):
        db_uri = kwargs.pop('db_uri')
        full_path = Path(db_uri).resolve()
        self.connection_string = f"sqlite+pysqlite:///{full_path}"
        super().__init__(**kwargs)

    async def on_load(self) -> None:
        await self.bind("q", "quit", "Quit")

    async def on_mount(self) -> None:
        # Create widgets
        self.metadata = ScrollView(auto_width=True)

        self.body = ScrollView(auto_width=True)

        self.browser = db.DataBrowser(self.connection_string)
        self.tree = TreeControl("Table Names", {})

        await self.view.dock(Header(), edge="top")
        await self.view.dock(Footer(), edge="bottom")

        await self.view.dock(self.metadata, edge="left", size=30, name="Metadata/Tables")
        await self.view.dock(self.body, edge="top")

        async def add_tables():
            table_names = self.browser.get_table_names()
            for name in table_names:
                await self.tree.add(self.tree.root.id, name, {"name": name})

            await self.tree.root.expand()
            await self.metadata.update(self.tree)

        await self.call_later(add_tables)

        async def add_dummy_body():
            table = Table("Dummy Table")
            table.add_column("Field")
            table.add_row("Data will appear once you select the table on the left side")

            await self.body.update(table)

        await self.call_later(add_dummy_body)

    async def handle_tree_click(self, message):
        async def add_content():
            batch = self.browser.get_table_rows(table_name=message.node.data['name'])
            table = batch.as_table()
            await self.body.update(table)

        await self.call_later(add_content)


def render(db_uri):
    Browser.run(title="Kombai - Data Browser", log="kombai.log", db_uri=db_uri)
